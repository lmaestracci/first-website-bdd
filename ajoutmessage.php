<?php

if(isset($_GET['Message']) && isset ($_GET['Pseudonyme'])){
    $message=$_GET['Message'];
    $pseudonyme=$_GET['Pseudonyme'];

    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'root');  
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    }

    $req = $bdd->prepare('INSERT INTO message (MESSAGE, PSEUDONYME) VALUES (:MESSAGE, :PSEUDONYME)');
    $req->execute(array(
        'MESSAGE' => $message,
        'PSEUDONYME' => $pseudonyme
    ));
    
    echo 'Le message a bien été ajouté !';
}


?>
